'use strict';

const gulp = require('gulp'),
      sass = require('gulp-sass'),
      sourcemaps = require('gulp-sourcemaps'),
      rename = require('gulp-rename'),
      autoprefixer = require('gulp-autoprefixer'),
      combineMq = require('gulp-combine-mq'),
      notify = require('gulp-notify'),
      plumber = require('gulp-plumber');


gulp.task('styles', () => {
 return gulp.src('../sass/main.scss')
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'nested'})

	.on('error', sass.logError)
  .on('error', (e) => {
		notify.onError({
			title: "Gulp",
			message: "SCSS compile error",
			sound: "Submarine"})(e);
	}))

  .pipe(autoprefixer({
    browsers: ['> 5%', 'last 2 versions'],
    cascade: false,
    remove: true
  }))

  .pipe(combineMq({
    beautify: true
  }))

  .pipe(rename('main.css'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('../css'))
});


gulp.task('watch', () => {
  gulp.watch('../sass/*.scss', ['styles']);
  gulp.watch('../sass/**/*.scss', ['styles']);
});


gulp.task('default', ['styles', 'watch']);
