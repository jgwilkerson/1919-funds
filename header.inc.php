<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>1919Funds.com | <?php echo$title;?></title>
		<meta name="keywords" content="<?php echo $keywords; ?>">
		<meta name="description" content="<?php echo $description;?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,300,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/animsition.min.css">
		<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	</head>
	<body>
		<div class="page-wrapper">
			<header>
				<div class="header-top">
					<div class="content">
						<div class="header-top--logo">
							<a href="/">
								<img src="images/1919-funds-logo.jpg" alt="1919 Funds logo" />
							</a>
						</div>

						<div class="header-top--links">
							<ul>
								<li>
									<a 
										href="#test-popup" 
										class="open-popup-link" 
										target="_blank" 
										onClick="ga('send', 'event', 'Popup Link', '1919ic.com', 'Popup');"
									>
											1919ic.com
									</a>
								</li>
								<li>
									<a 
										href="#test-popup2" 
										class="open-popup-link" 
										target="_blank" 
										onClick="ga('send', 'event', 'Popup Link', '1919strategies.com', 'Popup');"
									>
											1919strategies.com
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="subheader">
					<div class="subheader--image"></div>
					<div class="subheader--tagline">
						<div>
							<h2>1919 Funds</h2>
							<p>
								Offer a variety of investment solutions for investors 
								<br /> that have a strong alignment with their values and investment goals.
							</p>
						</div>
					</div>
				</div>
			</header>
			<nav class="navigation">
				<ul class="menu">
                   <li <?php if(isset($this_page) && $this_page == 'socially-responsive-balanced-fund') { echo 'class="current"';}  ?>><a href="index.php" title="Click here to go to 1919 Socially Responsive Balanced Fund">1919 Socially Responsive Balanced Fund</a></li>
					<li <?php if(isset($this_page) && $this_page == 'home') { echo 'class="current"';}  ?>><a href="financial-services-fund.php" title="Click here to go to 1919 Financial Services Fund ">1919 Financial Services Fund </a></li>
					<li <?php if(isset($this_page) && $this_page == 'maryland-tax-free-income-fund') { echo 'class="current"';}  ?>><a href="maryland-tax-free-income-fund.php" title="Click here to go to  1919 Maryland Tax-Free Income Fund">1919 Maryland Tax-Free Income Fund</a></li>
				</ul>
			</nav>