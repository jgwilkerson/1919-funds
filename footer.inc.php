<div id="test-popup" class="white-popup mfp-hide">
	<div class="warning-popup"><h5>You are about to leave the 1919funds.com website.</h5><a href="#" class="button popup-modal-dismiss" onClick="ga('send', 'event', 'Popup Button', 'Cancel Button', 'Cancel Button');">Cancel</a><a href="http://www.1919ic.com" class="button" target="_blank" onClick="ga('send', 'event', 'Popup Button', 'Accept Button', 'Accept Button');">Accept</a></div></div>


<div id="test-popup2" class="white-popup mfp-hide">
	<div class="warning-popup"><h5>You are about to leave the 1919funds.com website.</h5><a href="#" class="button popup-modal-dismiss" onClick="ga('send', 'event', 'Popup Button', 'Cancel Button', 'Cancel Button');">Cancel</a><a href="https://1919strategies.com/" class="button" target="_blank" onClick="ga('send', 'event', 'Popup Button', 'Accept Button', 'Accept Button');">Accept</a></div></div>




<footer>
	<div class="colophon">1919funds.com | All Rights Reserved | Copyright <?php echo date('Y'); ?> | 1.844.828.1919</div>
</footer>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="js/animsition.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39632817-2', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>