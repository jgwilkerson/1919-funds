<?php
$title = '1919 Socially Responsive Balanced Fund';
$keywords = 'Socially Responsive Balanced Fund, 1919 Investment Counsel, , asset man­agement firm, investment advisor, funds, fundamental research,  quality, risk management, diversification';
$description = 'The 1919 Socially Responsive Balanced Fund seeks a high total return through a socially responsive portfolio by identifying undervalued securities and then focusing on whether those issuers are conducting business in a socially responsive manner 1919 Investment Counsel, a globally recognized asset man­agement firm, is the investment advisor to the Funds. ';
$this_page = 'socially-responsive-balanced-fund';
include 'header.inc.php';
?>
<main class="main">
    <div class="content">
        <div id="tabbed-content" class="animsition">
            <section>
                <div class="intro">
                    <div class="intro-content">
                        <h3>1919 Socially Responsive Balanced Fund</h3>
                        <p>The Fund seeks a high total return through a socially responsive portfolio by identifying undervalued securities and then focusing on whether those issuers are conducting business in a socially responsive manner. The Fund aims to invest 70% of total assets in U.S. stocks and 30% in investment-grade U.S. debt, and it may also invest in foreign stocks and debt.</p>
                    </div>
                </div>
            </section>
            <section>
                <div class="fund-info">
                    <div class="left-content">
                        <h4><a href="pdfs/1919-Socially-Responsive-Balanced-Fund-Fact-Sheet.pdf" target="_blank" title="Click here for the Fact Sheet" onClick="ga('send', 'event', 'PDF', 'Socially Reponsive Balanced Fund Fact Sheet Download', 'Fact Sheet Download');">Fact Sheet <i class="fa fa-file-pdf-o"></i></a></h4>
                    </div>
                    <div class="right-content">
                        <h4><a href="pdfs/1919-Socially-Responsive-Balanced-Fund-Commentary.pdf" target="_blank" title="Click here for the Manager Commentary" onClick="ga('send', 'event', 'PDF', 'Socially Reponsive Balanced Fund Manager Commentary Download', 'Manager Commentary Download');">Manager Commentary <i class="fa fa-file-pdf-o"></i></a></h4>
                    </div>
                </div>
            </section>
            <section>
                <div class="fund-info">
                    <div class="left-content" id="additional-info-box">
                        <h4>Additional Information</h4>
                        <p><a href="pdfs/all/1919-Funds-Statutory-Prospectus.pdf" target="_blank" title="Click here for the Prospectus " onClick="ga('send', 'event', 'PDF', 'SRBF Page Prospectus Download', 'Prospectus Download');">Prospectus <i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Socially-Responsible-Summary-Prospectus.pdf" target="_blank" title="Click here for Summary Prospectus" onClick="ga('send', 'event', 'PDF', 'SRBF Summary Prospectus Download', 'Summary Prospectus Download');">Summary Prospectus<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Funds-SAI.pdf" target="_blank" title="Click here for the Statement of Additional Information" onClick="ga('send', 'event', 'PDF', 'SRBF Page SAI Download', 'SAI Download');">Statement of Additional Information <i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Sales-Charges-and-Breakpoints.pdf" target="_blank" title="Click here for Sales Charges and Breakpoints" onClick="ga('send', 'event', 'PDF', 'Sales Charges Download', 'Sales Charges Download');">Sales Charges and Breakpoints<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="https://vds.issgovernance.com/vds/#/Mjg2Mw==/" target="_blank" title="Click here for Proxy Voting Information">Click here for Proxy Voting Information</a></p>
                        <p><a href="pdfs/all/1919-Funds-Annual-Report.pdf" target="_blank" title="Click here for the Annual Report" onClick="ga('send', 'event', 'PDF', 'SRBF Page Annual Report Download', 'Annual Report Download');">Annual Report <i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Funds-Semi-Annual-Report.pdf" target="_blank" title="Click here for the Semi-Annual Report" onClick="ga('send', 'event', 'PDF', 'SRBF Page Semi Annual Report Download', 'Semi Annual Report Download');">Semi-Annual Report <i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Funds-XBRL.zip" target="_blank" title="Click here for the XBRL Zip File Download" onClick="ga('send', 'event', 'PDF', 'SRBF Page XBRL Zip File Download', 'XBRL Zip File Download');">1919 Funds XBRL <i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Funds-IRA-Disclosure.pdf" target="_blank" title="Click here for the IRA Disclosure Download" onClick="ga('send', 'event', 'PDF', 'SRBF Page IRA Disclosure Download', 'IRA Disclosure Download');">IRA Disclosure<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-IRA-Application.pdf" target="_blank" title="Click here to download the IRA Disclosure" onClick="ga('send', 'event', 'PDF', 'FS Page IRA Disclosure Download', 'IRA Disclosure Download');">IRA Application<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-IRA-Transfer-Form.pdf" target="_blank" title="Click here to download the IRA Disclosure" onClick="ga('send', 'event', 'PDF', 'FS Page IRA Disclosure Download', 'IRA Disclosure Download');">IRA Transfer Form<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Coverdell-Education-Savings-Application.pdf" target="_blank" title="Click here to download the IRA Disclosure" onClick="ga('send', 'event', 'PDF', 'FS Page IRA Disclosure Download', 'IRA Disclosure Download');">Coverdell Education Savings Application<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-New-Account-Application.pdf" target="_blank" title="Click here to download the IRA Disclosure" onClick="ga('send', 'event', 'PDF', 'FS Page IRA Disclosure Download', 'IRA Disclosure Download');">New Account Application (non IRA)<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Certification-of-Beneficial-Owner.pdf" target="_blank" title="Click here to download the Certification of Beneficials Owner" onClick="ga('send', 'event', 'PDF', 'FS Page IRA Disclosure Download', 'IRA Disclosure Download');">Certification of Beneficial Owner<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Entity-Account-Application.pdf" target="_blank" title="Click here to download the Entity Account Application" onClick="ga('send', 'event', 'PDF', 'FS Page IRA Disclosure Download', 'IRA Disclosure Download');">Entity Account Application<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                        <p><a href="pdfs/all/1919-Capital-Gains.pdf" target="_blank" title="Click here for the Capital Gains Download" onClick="ga('send', 'event', 'PDF', 'SRBF  Page Capital Gains Download', 'Capital Gains Download');">2018 Capital Gains Distributions<i class="fa fa-file-pdf-o added-info"></i></a>
                        </p>
                    </div>
                    <div class="company-content">
                        <?php include 'company-summary.inc.php' ?>
                        <?php include 'contact-info.inc.php' ?>
                    </div>
                </div>
            </section>
            <?php include 'disclosure.inc.php';?>
        </div>
    </div>
</main>

<?php include 'footer.inc.php'; ?>